import React from 'react';
import './App.css';

import Login from './pages/Login';
import Register from './pages/Register';
import Aplikasi from './pages/Aplikasi';
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import Loader from './components/Loader';


function App() {
  return (
    <div className="App">
      <Loader/>
       <Router>
          <Route path="/list">
            <div>
              <ol>
                <li>
                  <Link to="/login">Login</Link>
                </li>
                <li>
                  <Link to="/Regiter">Register</Link>
                </li>
                <li>
                  <Link to="/aplikasi">Aplikasi</Link>
                </li>
              </ol>
            </div>
          </Route>
          <div className="container">
            <Switch>
              <Route path="/" exact component={Login} />
              <Route path="/register" component={Register} />
              <Route path="/aplikasi" component={Aplikasi} />
            </Switch>
        </div>
        </Router>

      {/* baseUrl : http://18.141.178.15:8080/
      registerUrl :  http://18.141.178.15:8080/registerUrl
      loginUrl: 
      register: 
        username: mancecep
        password: 123456
        email: cecepsolihinyusup@gmail.com
      login:
        username: mancecep
        password: 123456 */}
    </div>
  );
}

export default App;
