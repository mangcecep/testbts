import React from 'react';
import { Link } from "react-router-dom";
import axios from "axios";

const baseUrl = "http://18.141.178.15:8080/login";


const Login = () => {
    const [login, setLogin] = React.useState({
        username: '',
        password: ''
    });
    
    const handleSubmit = (event) => {
            setLogin(
                aaxios.post(baseUrl, {
                params : {
                    username: event.target.value,
                    password: event.target.value
                }
            }).then(res => {
                console.log(res);
            }).catch(err => {
                console.log(err);
            })
            )
        }

    return (
        <div className="authentication">
    <div className="container">
        <div className="row">
            <div className="col-lg-4 col-sm-12">
                <form 
                    className="card auth_form"
                    onSubmit={handleSubmit}>
                    <div className="header">
                        <img className="logo" src="assets/images/logo.svg" alt="logo"/>
                        <h5>Log in</h5>
                    </div>
                    <div className="body">
                        <div className="input-group mb-3">
                            <input type="text" 
                            className="form-control" 
                            placeholder="Username"
                            name="username"
                            // onChange={handleChange}
                            />
                            <div className="input-group-append">
                                <span className="input-group-text"><i className="zmdi zmdi-account-circle"></i></span>
                            </div>
                        </div>
                        <div className="input-group mb-3">
                            <input type="text"
                            className="form-control"
                            placeholder="Password"
                            name="password"
                            // onChange={handleChange}
                            />
                            <div className="input-group-append">                                
                                <span className="input-group-text"><a href="forgot-password.html" className="forgot" title="Forgot Password"><i className="zmdi zmdi-lock"></i></a></span>
                            </div>                            
                        </div>
                        <div className="checkbox">
                            <input id="remember_me" type="checkbox"/>
                            <label htmlFor="remember_me">Remember Me</label>
                        </div>
                        <button type="submit" className="btn btn-primary btn-block waves-effect waves-light">SIGN IN</button>
                    </div>
                </form>
                <p>belum punya akun? <Link to="/register">Daftar</Link></p>
            </div>
            <div className="col-lg-8 col-sm-12">
                <div className="card">
                    <img src="/assets/images/signin.svg" alt="Sign In"/>
                </div>
            </div>
        </div>
    </div>
</div>
    )
}

export default Login
